#!/bin/bash

PROJECT_2_4_ID=36278526

curl --header "PRIVATE-TOKEN: $PRIVATE_TOKEN" "https://gitlab.com/api/v4/projects/35967784/jobs" | \
 jq '.[] | select(.name == "build-prod") | select(.status=="failed") | .id' | head -1

curl --request POST \
  --form token=$PIPE_TRIGGER_2_4 \
  --form ref=main \
  --form "variables[NGINX_PORT]=$NGINX_PORT" \
  --form "variables[PROJECT_URL]=$PROJECT_URL" \
  --form "variables[BRANCH]=$BRANCH" \
  --form "variables[PROJ_2_1_COMMIT_SHA]=$CI_COMMIT_SHORT_SHA" \
  --form "variables[PROJ_2_1_JOB_ID]=$CI_JOB_ID" \
  --form "variables[PROJ_2_1_JOB_TOKEN]=$CI_JOB_TOKEN" \
  --form "variables[PROJ_2_1_ID]=$CI_PROJECT_ID" \
  "https://gitlab.com/api/v4/projects/${PROJECT_2_4_ID}/trigger/pipeline"

sleep 60

CI_PIPELINE_ID=$(curl --header "PRIVATE-TOKEN: $PRIVATE_TOKEN" https://gitlab.com/api/v4/projects/${PROJECT_2_4_ID}/pipelines | jq '.[0] | .id')
JOB_ID=$(curl --header "PRIVATE-TOKEN: $PRIVATE_TOKEN" https://gitlab.com/api/v4/projects/${PROJECT_2_4_ID}/pipelines/${CI_PIPELINE_ID}/jobs | jq '.[0] | .id')

curl --request POST --header "PRIVATE-TOKEN: $PRIVATE_TOKEN" "https://gitlab.com/api/v4/projects/${PROJECT_2_4_ID}/jobs/${JOB_ID}/play"