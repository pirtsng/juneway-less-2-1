#!/bin/bash
# wait for db to initialize
while ! nc -z db 5432; do sleep 2; done
# migrate and run app
python manage.py migrate 
/usr/local/bin/gunicorn Blog.wsgi:application -b :8000 --reload